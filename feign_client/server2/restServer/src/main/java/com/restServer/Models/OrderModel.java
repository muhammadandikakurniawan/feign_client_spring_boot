package com.restServer.Models;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.*;

@Entity
@Table(name = "tb_order", schema = "training")
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer Id;

    @Column(name="sku_code")
    private String SkuCode;

    @Column(name="user_id")
    private Integer UserId;
    
}