package com.restServer.Models;

import javax.persistence.*;

import lombok.*;

@Entity
@Table(name = "tb_user", schema = "training")
@Getter
@Setter
public class UserModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer Id;

    @Column(name="name")
    private String Name;

    @Column(name="email")
    private String Email;

    @Column(name="password")
    private String Password;

}
