package com.restServer.Models;

import javax.persistence.*;

import lombok.*;

@Entity
@Table(name = "tb_product", schema = "training")
@Getter
@Setter
public class ProductModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer Id;

    @Column(name="name")
    private String Name;

    @Column(name="sku_code")
    private String SkuCode;

    @Column(name="price")
    private String Price;
}
