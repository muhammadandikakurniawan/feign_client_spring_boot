package com.restServer.Repository;

import com.restServer.Dto.*;
import com.restServer.Models.*;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.*;

import javax.persistence.NamedNativeQueries;

@Repository
public interface IOrderRepo extends JpaRepository<OrderModel, Integer>{
    
    

}
