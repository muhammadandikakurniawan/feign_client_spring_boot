package com.restServer.Dto;

import com.restServer.Models.*;

import lombok.*;

@Getter
@Setter
public class OrderDto {
    
    public OrderDto(UserModel u, ProductModel p) {
        this.User = u;
        this.Product = p;
    }

    private UserModel User;
    private ProductModel Product;
}
