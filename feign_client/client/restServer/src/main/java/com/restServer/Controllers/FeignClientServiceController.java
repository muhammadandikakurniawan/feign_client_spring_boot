package com.restServer.Controllers;

import java.util.HashMap;
import java.util.List;

import com.restServer.Dto.OrderDto;
import com.restServer.FeignClientService.IFeignClientService;
import com.restServer.Proxy.IFeignClientProxy;

import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("TestFeignClient")
@EnableFeignClients(basePackageClasses = IFeignClientProxy.class)
public class FeignClientServiceController implements IFeignClientProxy {

    private IFeignClientProxy _feignProxy;

    public FeignClientServiceController(IFeignClientProxy feignProxy){
        this._feignProxy = feignProxy;
    }

    @Override
    @RequestMapping(value = "GetOrderDetail", method = RequestMethod.POST)
    public List<OrderDto> GetOrderDetail() {
        return this._feignProxy.GetOrderDetail();
    }

    @Override
    @RequestMapping(value = "GetOrderDetailBySkuCode", method = RequestMethod.POST)
    public List<OrderDto> GetOrderDetailBySkuCode(HashMap<String, Object> param) {
        return this._feignProxy.GetOrderDetailBySkuCode(param);
    }
    
}
