package com.restServer.Repository;

import com.restServer.Models.*;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserRepo extends JpaRepository<UserModel,Integer>{
    
}
